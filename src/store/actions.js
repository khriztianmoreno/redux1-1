
/**
 *
 * @param {*} count
 */
export const acAdd = count => {
  return {
    type: 'ADD',
    payload: count
  }
}

export const acRemove = count => {
  return {
    type: 'REMOVE',
    payload: count
  }
}

export const acMessage = msg => {
  return {
    type: 'REMOVE',
    payload: msg
  }
}


