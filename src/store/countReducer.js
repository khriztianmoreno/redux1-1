// Actions
// Flux Standard Action (FSA)
// { type: 'ADD', payload: {}   }


const initialState = {
  count: 1,
  messages: ''
}

/**
 *
 * @param {*} state
 * @param {*} action
 */
const countReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD': {
      return {
        ...state,
        count: action.payload
      }
    }
    case 'REMOVE' : {
      return {
        ...state,
        count: action.payload
      }
    }
    default:
      return initialState
  }
}

export default countReducer
