import React from 'react'
import { connect } from 'react-redux'

import { acAdd, acRemove, acMessage } from '../store/actions'

class Main extends React.PureComponent {
  state = {
    limit: 10,
    message: ''
  }

  handlerButton = () => {
    const { agregar, count } = this.props
    const accum = count + 1
    agregar(accum)

    if (count > this.state.limit) {
      //
    } else {
      this.setState({ message: 'Bien' })
    }
  }

  render() {
    const { count, message } = this.props
    return (
      <div>
        <h1>{count}</h1>
        <p>{this.state.message}</p>
        <button onClick={this.handlerButton}>Increm</button>
        <button onClick={() => {this.props.reducir(count-1)}}>Decrem</button>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    count: state.countReducer.count,
    message: state.countReducer.message,
  }
}

const mapDipatchToProps = (dispatch) => {
  return {
    agregar: (value) => dispatch(acAdd(value)),
    reducir: (value) => dispatch(acRemove(value)),
    newMessage: (msg) => dispatch(acMessage(msg)),
  }
}

export default connect(mapStateToProps, mapDipatchToProps)(Main)
