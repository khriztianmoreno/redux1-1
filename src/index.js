import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux'
import { Provider } from 'react-redux'

import rootReducer from './store'

import './index.css';
import App from './App';

const myStore = createStore(rootReducer,  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

const Root = () => (
  <Provider store={myStore}>
    <App />
  </Provider>
)


ReactDOM.render(<Root />, document.getElementById('root'));
